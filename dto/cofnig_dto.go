package dto

type ConfigDto struct {
	Bottoken               string
	Dbconnection           string
	Dbmigrationsconnection string
	Servicedchat           int64
	Connectionport         string
}
