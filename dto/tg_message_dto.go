package dto

import (
	"context"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type UpdatesChannel <-chan TgMessageDto

type TgMessageDto struct {
	Update  tgbotapi.Update
	Context context.Context
}
