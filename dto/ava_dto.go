package dto

type AvaDto struct {
	Id       int
	UserId   string
	ImageId  string
	WasUsing bool
}
