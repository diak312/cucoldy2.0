package main

import (
	"fmt"
	"net/http"

	bootstrap "avaBot/internal"
)

func main() {
	fmt.Println("Server started!")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Hello all!")
	})

	telegramInitialiser := bootstrap.TelegramControllerInitialiser{}
	err := telegramInitialiser.Init()

	if err != nil {
		fmt.Println(err)
	}

	//pipline test11
}
