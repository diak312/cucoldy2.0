package bootstrap

import (
	"avaBot/dto"
	"avaBot/repositories"
)

func GetAvaRepository(config dto.ConfigDto) (*repositories.AvaRealisationRepository, error) {
	dhInitialiser := DBInitialiser{}
	err := dhInitialiser.Init(config)

	if err != nil {
		return nil, err
	}

	poolConnections := dhInitialiser.GetPool()

	return &repositories.AvaRealisationRepository{poolConnections}, nil
}
