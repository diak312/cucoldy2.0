package bootstrap

import (
	"avaBot/dto"
	"fmt"

	_ "github.com/lib/pq"
	"github.com/pressly/goose"
)

func InitMigrations(config dto.ConfigDto) error {
	fmt.Println(config.Dbmigrationsconnection)

	db, err := goose.OpenDBWithDriver("postgres", config.Dbmigrationsconnection)
	if err != nil {
		fmt.Println(err)
	}

	defer func() {
		if err := db.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	if err := goose.Run("up", db, "migrations"); err != nil {
		fmt.Println(err)
	}

	return nil
}
