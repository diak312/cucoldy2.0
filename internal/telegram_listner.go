package bootstrap

import (
	"avaBot/dto"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func ListenTelegramUpdates(bot *tgbotapi.BotAPI) (*tgbotapi.UpdatesChannel, error) {
	updates := bot.ListenForWebhook("/webhook")

	go http.ListenAndServe(":80", nil)

	fmt.Println("Telegram listner started!")

	return &updates, nil
}

func getTelegramUpdates(pattern string, config dto.ConfigDto) dto.UpdatesChannel {
	ch := make(chan dto.TgMessageDto)

	http.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		bytes, _ := ioutil.ReadAll(r.Body)

		var update tgbotapi.Update
		json.Unmarshal(bytes, &update)

		messageContext := context.Background()

		ch <- dto.TgMessageDto{
			Update:  update,
			Context: messageContext,
		}
	})

	go http.ListenAndServe(":"+config.Connectionport, nil)

	fmt.Println("Telegram listner started!")

	return ch
}
