package bootstrap

import (
	"fmt"

	"avaBot/dto"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func InitTelegramApi(config dto.ConfigDto) (*tgbotapi.BotAPI, error) {
	bot, err := configureBot(config.Bottoken)

	if err != nil {
		return nil, err
	}

	err = setWebhookBot(bot)

	if err != nil {
		return nil, err
	}

	return bot, nil
}

func configureBot(telegramToken string) (*tgbotapi.BotAPI, error) {
	bot, err := tgbotapi.NewBotAPI(telegramToken)
	if err != nil {
		return nil, err
	}

	bot.Debug = true
	fmt.Println("Authorized on telegram account " + bot.Self.UserName)

	return bot, nil
}

func setWebhookBot(bot *tgbotapi.BotAPI) error {
	_, err := bot.SetWebhook(tgbotapi.NewWebhook("https://diakov312.gb.net:443/webhook"))
	if err != nil {
		return err
	}

	fmt.Println("Telegram webhook set!")

	/* info, err := bot.GetWebhookInfo()
	if err != nil {
		fmt.Println(err)
	}

	if info.LastErrorDate != 0 {
		fmt.Println("Telegram callback failed: " + info.LastErrorMessage)
	} */

	return nil
}
