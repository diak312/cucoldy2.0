package bootstrap

import (
	"avaBot/controllers"
	"avaBot/services"
)

type TelegramControllerInitialiser struct {
}

func (telegramControllerInitialiser *TelegramControllerInitialiser) Init() error {

	config, err := InitConfig()
	if err != nil {
		return err
	}

	err = InitMigrations(config)
	if err != nil {
		return err
	}

	avaRepository, err := GetAvaRepository(config)
	if err != nil {
		return err
	}

	botApi, err := InitTelegramApi(config)
	if err != nil {
		return err
	}

	/* telegramUpdates, err := ListenTelegramUpdates(botApi)
	if err != nil {
		return err
	} */

	telegramUpdates := getTelegramUpdates("/webhook", config)

	telegramPhotoHandlerService := services.TelegramPhotoHandlerService{
		Config:        config,
		AvaRepository: avaRepository,
		Bot:           botApi,
	}
	telegramCommandHandlerService := services.TelegramCommandHandlerService{
		AvaRepository: avaRepository,
		PhotoHandler:  telegramPhotoHandlerService,
	}

	/* cronController := controllers.CronController{
		Bot:                 botApi,
		PhotoHandlerService: telegramPhotoHandlerService,
	}
	cronController.Init() */

	messageController := controllers.MessageController{
		Bot:                   botApi,
		Updates:               telegramUpdates,
		CommandHandlerService: telegramCommandHandlerService,
		PhotoHandlerService:   telegramPhotoHandlerService,
	}
	err = messageController.Init()
	if err != nil {
		return err
	}

	return nil
}
