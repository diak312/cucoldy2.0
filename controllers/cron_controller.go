package controllers

import (
	"context"
	"fmt"

	"avaBot/dto"
	"avaBot/services"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/robfig/cron/v3"
)

type CronController struct {
	Bot                 *tgbotapi.BotAPI
	PhotoHandlerService services.TelegramPhotoHandlerService
}

func (controller *CronController) Init() {
	c := cron.New()
	c.AddFunc("*/1 * * * *", controller.ChangeAva)
	c.Start()
}

func (controller *CronController) ChangeAva() {
	newContext := context.Background()
	err := controller.PhotoHandlerService.ChangeAva(dto.TgMessageDto{Context: newContext})
	if err != nil {
		fmt.Println(err)
	}
}
