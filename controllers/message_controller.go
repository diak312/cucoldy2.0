package controllers

import (
	"avaBot/dto"
	"avaBot/services"
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type MessageController struct {
	Bot                   *tgbotapi.BotAPI
	Updates               dto.UpdatesChannel
	CommandHandlerService services.TelegramCommandHandlerService
	PhotoHandlerService   services.TelegramPhotoHandlerService
}

func (controller *MessageController) Init() error {

	for update := range controller.Updates {
		go controller.handleUpdate(update)
	}
	//for update := range *controller.Updates {

	/* if update.Message.IsCommand() {
		messageResponse, err := controller.CommandHandlerService.HandleCommand(*update.Message)

		if err != nil {
			return err
		}

		if messageResponse != nil {
			controller.Bot.Send(messageResponse)
		}
	}

	if update.Message.Photo != nil {
		photoResponse, err := controller.PhotoHandlerService.HandlePhoto(update)

		if err != nil {
			return err
		}

		if photoResponse != nil {

		}
	} */

	/* 		file, error := bot.GetFile(tgbotapi.FileConfig{photo.FileID})

	   		if error != nil {
	   			fmt.Println("")
	   		}

	   		url := "https://api.telegram.org/file/bot_BOT_TOKEN_/" + file.FilePath

	   		fileBytes, err := downloadFile(url)

	   		if err != nil {
	   			fmt.Println("ERROR DOWNLOAD")
	   			fmt.Println(fileBytes)
	   			fmt.Println("ERROR DOWNLOAD")
	   		}

	   		// photoResend := tgbotapi.NewPhotoShare(-424131203, photo.FileID)

	   		fileUpload := tgbotapi.FileBytes{Name: "test4", Bytes: fileBytes}

	   		ava := tgbotapi.NewSetChatPhotoUpload(-424131203, fileUpload)

	   		response, errResp := bot.SetChatPhoto(ava)

	   		if errResp != nil {
	   			fmt.Println("ERROR RESPONSE")
	   			fmt.Println(errResp)
	   			fmt.Println("ERROR RESPONSE")
	   		}

	   		fmt.Println("-----------------")
	   		fmt.Println(response)
	   		fmt.Println("-----------------") */

	// bot.Send(photoResend)
	//}

	return nil
}

func (controller *MessageController) handleUpdate(tgMessage dto.TgMessageDto) {
	if tgMessage.Update.Message.IsCommand() {

		messageResponse, err := controller.CommandHandlerService.HandleCommand(tgMessage)

		if err != nil {
			fmt.Println(err)
		}

		if messageResponse != nil {
			controller.Bot.Send(messageResponse)
		}
	}

	if tgMessage.Update.Message.Photo != nil {
		photoResponse, err := controller.PhotoHandlerService.HandlePhoto(tgMessage)

		if err != nil {
			fmt.Println(err)
		}

		if photoResponse != nil {

		}
	}

	//return nil
}
