-- +goose Up
CREATE TABLE IF NOT EXISTS avatars
(
	id SERIAL NOT NULL
		CONSTRAINT avatars_pk
			PRIMARY KEY,
	user_id TEXT NOT NULL,
	image_id TEXT NOT NULL,
	was_using BOOLEAN DEFAULT false NOT NULL,
	created_at TIMESTAMP DEFAULT NOW() NOT NULL,
	updated_at TIMESTAMP
);

COMMENT ON COLUMN avatars.user_id IS 'who added image';
-- +goose StatementBegin
SELECT 'up SQL query';
-- +goose StatementEnd

-- +goose Down
DROP TABLE avatars;
-- +goose StatementBegin
SELECT 'down SQL query';
-- +goose StatementEnd
