package services

import (
	"errors"
	"io/ioutil"
	"net/http"

	"avaBot/dto"
	"avaBot/repositories"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type TelegramPhotoHandlerService struct {
	Config        dto.ConfigDto
	Bot           *tgbotapi.BotAPI
	AvaRepository repositories.AvaRepository
}

func (service *TelegramPhotoHandlerService) HandlePhoto(tgMessage dto.TgMessageDto) (tgbotapi.Chattable, error) {
	if tgMessage.Update.Message.Chat.IsPrivate() {
		var photoID string
		var userName string
		sendedPhoto := *tgMessage.Update.Message.Photo

		if len(sendedPhoto) != 0 {
			photoID = sendedPhoto[0].FileID
		} else {
			return nil, errors.New("Can't get photo!")
		}

		if tgMessage.Update.Message.From != nil {
			userName = tgMessage.Update.Message.From.UserName
		} else {
			return nil, errors.New("Can't get 'from' message information!")
		}

		err := service.AvaRepository.AddAva(userName, photoID, tgMessage.Context)

		if err != nil {
			return nil, err
		}

		return tgbotapi.NewMessage(tgMessage.Update.Message.Chat.ID, ""), nil
	}

	return nil, nil
}

func (service *TelegramPhotoHandlerService) ChangeAva(tgMessage dto.TgMessageDto) error {
	ava, err := service.AvaRepository.GetRandomAva(tgMessage.Context)
	if err != nil {
		if err.Error() == "no rows in result set" {
			service.Bot.Send(tgbotapi.NewMessage(service.Config.Servicedchat, "Can't get ava from database!"))
			return errors.New("Can't get ava from database!")
		}

		return err
	}

	if ava == nil {
		service.Bot.Send(tgbotapi.NewMessage(service.Config.Servicedchat, "Can't get ava from database!"))
		return errors.New("Can't get ava from database!")
	}

	photoBytes, err := service.DownloadPhoto(ava.ImageId)
	if err != nil {
		return err
	}

	err = service.SetChatPhoto(ava.ImageId, photoBytes)
	if err != nil {
		return err
	}

	service.AvaRepository.SetAvaUsed(ava.Id, tgMessage.Context)
	if err != nil {
		return err
	}

	return nil
}

func (service *TelegramPhotoHandlerService) DownloadPhoto(fileId string) ([]byte, error) {
	fileConfig := tgbotapi.FileConfig{FileID: fileId}

	file, err := service.Bot.GetFile(fileConfig)

	if err != nil {
		return nil, err
	}

	url := "https://api.telegram.org/file/bot" + service.Config.Bottoken + "/" + file.FilePath

	fileBytes, err := downloadFile(url)

	if err != nil {
		return nil, err
	}

	return fileBytes, nil
}

func (service *TelegramPhotoHandlerService) SetChatPhoto(fileName string, fileBytes []byte) error {
	fileUpload := tgbotapi.FileBytes{Name: fileName, Bytes: fileBytes}
	ava := tgbotapi.NewSetChatPhotoUpload(service.Config.Servicedchat, fileUpload)

	_, err := service.Bot.SetChatPhoto(ava)

	if err != nil {
		return err
	}

	return nil
}

func downloadFile(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()
	newBody, err := ioutil.ReadAll(response.Body)

	return newBody, err
}
