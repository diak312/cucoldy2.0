package services

import (
	"fmt"
	"strconv"

	"avaBot/dto"
	"avaBot/repositories"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type TelegramCommandHandlerService struct {
	AvaRepository repositories.AvaRepository
	PhotoHandler  TelegramPhotoHandlerService
}

func (service *TelegramCommandHandlerService) HandleCommand(tgMessage dto.TgMessageDto) (tgbotapi.Chattable, error) {

	switch tgMessage.Update.Message.Command() {
	case "start", "help":
		messageText := "Для добавления аватарок просто передайте набор картинок боту в личку." +
			" Для удаления добавленных картинок укажите команду /delete <число удаляемых картинок>." +
			" Для показа количества добавленных картинок используйте /countAva."

		return tgbotapi.NewMessage(tgMessage.Update.Message.Chat.ID, messageText), nil
	case "test":
		return tgbotapi.NewMessage(tgMessage.Update.Message.Chat.ID, "Get command "+tgMessage.Update.Message.Command()), nil
	case "getOldAva":
		avaDto, err := service.AvaRepository.GetRandomAva(tgMessage.Context)

		if err != nil {
			fmt.Println("Can't get ava!")
			fmt.Println(err)
		}

		return tgbotapi.NewPhotoShare(tgMessage.Update.Message.Chat.ID, avaDto.ImageId), nil
	case "delete":
		commandArgument := tgMessage.Update.Message.CommandArguments()
		countDeleted, err := strconv.Atoi(commandArgument)
		if err != nil || countDeleted <= 0 {
			return tgbotapi.NewMessage(tgMessage.Update.Message.Chat.ID, "Invalid argument format!"), nil
		}

		fromUser := tgMessage.Update.Message.From.UserName

		err = service.AvaRepository.DeleteLastUserAva(countDeleted, fromUser, tgMessage.Context)
		if err != nil {
			return nil, err
		}

		return tgbotapi.NewMessage(tgMessage.Update.Message.Chat.ID, "Count: "+strconv.Itoa(countDeleted)+". User: "+fromUser), nil
	case "countAva":
		fromUser := tgMessage.Update.Message.From.UserName
		countAddedAva, err := service.AvaRepository.GetCountNotUsedAva(fromUser, tgMessage.Context)
		if err != nil {
			return nil, err
		}

		return tgbotapi.NewMessage(tgMessage.Update.Message.Chat.ID, "Count avatars added "+fromUser+": "+strconv.Itoa(countAddedAva)), nil
	case "changeChatPhoto":
		err := service.PhotoHandler.ChangeAva(tgMessage)
		if err != nil {
			return nil, err
		}

		return nil, nil
	}

	return nil, nil
}
