module avaBot

go 1.14

require (
	github.com/ClickHouse/clickhouse-go v1.4.3 // indirect
	github.com/denisenkom/go-mssqldb v0.9.0 // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/jackc/pgx/v4 v4.9.0
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.9.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pressly/goose v2.7.0+incompatible // indirect
	github.com/robfig/cron/v3 v3.0.1
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/ziutek/mymysql v1.5.4 // indirect
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
)
