package repositories

import (
	"context"
	"fmt"

	"avaBot/dto"

	"github.com/jackc/pgx/v4/pgxpool"
)

type AvaRealisationRepository struct {
	PoolConnections *pgxpool.Pool
}

func (repository *AvaRealisationRepository) AddAva(userID string, imageID string, context context.Context) error {
	query := "INSERT INTO avatars (user_id, image_id) VALUES ($1, $2)"

	conn, err := repository.PoolConnections.Acquire(context)
	if err != nil {
		return err
	}

	connection := conn.Conn()

	_, err = connection.Exec(context, query, userID, imageID)

	if err != nil {
		return err
	}

	conn.Release()

	return nil
}

func (repository *AvaRealisationRepository) GetRandomAva(context context.Context) (*dto.AvaDto, error) {
	var avaDto *dto.AvaDto
	avaDto = new(dto.AvaDto)
	queryString := "SELECT id, user_id, image_id, was_using FROM avatars WHERE was_using = false ORDER BY random() LIMIT 1"

	conn, err := repository.PoolConnections.Acquire(context)
	if err != nil {
		fmt.Println("Unable to acquire a database connection: %v\n", err)
	}

	err = conn.Conn().QueryRow(context, queryString).Scan(&avaDto.Id, &avaDto.UserId, &avaDto.ImageId, &avaDto.WasUsing)
	if err != nil {
		return nil, err
	}

	conn.Release()

	return avaDto, nil
}

func (repository *AvaRealisationRepository) SetAvaUsed(avaID int, context context.Context) error {
	query := "UPDATE avatars SET was_using = true WHERE id = $1"

	conn, err := repository.PoolConnections.Acquire(context)
	if err != nil {
		return err
	}

	connection := conn.Conn()

	_, err = connection.Exec(context, query, avaID)

	if err != nil {
		return err
	}

	conn.Release()

	return nil
}

func (repository *AvaRealisationRepository) DeleteLastUserAva(countDeleted int, userName string, context context.Context) error {
	query := "DELETE FROM avatars WHERE ctid IN (SELECT ctid FROM avatars WHERE user_id = $1 AND was_using = false ORDER BY created_at DESC LIMIT $2)"

	conn, err := repository.PoolConnections.Acquire(context)
	if err != nil {
		return err
	}

	connection := conn.Conn()

	_, err = connection.Exec(context, query, userName, countDeleted)

	if err != nil {
		return err
	}

	conn.Release()

	return nil
}

func (repository *AvaRealisationRepository) GetCountNotUsedAva(userID string, messageContext context.Context) (int, error) {
	count := 0
	queryString := "SELECT COUNT(id) FROM avatars WHERE was_using = false AND user_id=$1"

	conn, err := repository.PoolConnections.Acquire(context.Background())

	if err != nil {
		fmt.Println("Unable to acquire a database connection: %v\n", err)
	}

	err = conn.Conn().QueryRow(context.Background(), queryString, userID).Scan(&count)
	if err != nil {
		return 0, err
	}

	conn.Release()

	return count, nil
}
