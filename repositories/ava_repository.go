package repositories

import (
	"context"

	"avaBot/dto"
)

type AvaRepository interface {
	AddAva(userID string, imageID string, context context.Context) error
	GetRandomAva(context context.Context) (*dto.AvaDto, error)
	GetCountNotUsedAva(userID string, context context.Context) (int, error)
	SetAvaUsed(avaID int, context context.Context) error
	DeleteLastUserAva(countDeleted int, userName string, context context.Context) error
}
